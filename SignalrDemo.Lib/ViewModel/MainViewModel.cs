using GalaSoft.MvvmLight;
using Microsoft.AspNet.SignalR.Client.Hubs;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SignalrDemo.Lib.Data;
using SignalrDemo.Lib.Messaging;

namespace SignalrDemo.Lib.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        //private ObservableCollection<Message> Messages { get; set; }
        private ConnectionState _connectionState;

        public ConnectionState ConnectionState
        {
            get { return _connectionState; }
            set
            {
                if (_connectionState == value) return;
                _connectionState = value;
                RaisePropertyChanged("ConnectionState");
                RaisePropertyChanged("IsConnected");
            }
        }

        public bool IsConnected
        {
            get { return ConnectionState == ConnectionState.Connected; }
        }

        public MainViewModel()
        {
            //Items = new ObservableCollection<Quote>();
            Messages = new ObservableCollection<Message>();

            MessengerInstance.Register<ChatReceivedMessage>(this, ReceivedMessageHandler);
            MessengerInstance.Register<ConnectionStateChangedMessage>(this,
                                                                      ConnectionStateChangedHandler);
        }

        private void ConnectionStateChangedHandler(ConnectionStateChangedMessage msg)
        {
            ConnectionState = msg.NewState;
        }

        private void ReceivedMessageHandler(ChatReceivedMessage msg)
        {
            var message = msg.message;
            Messages.Add(message);
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<Message> Messages { get; private set; }

        public override void Cleanup()
        {
            MessengerInstance.Unregister(this);
            base.Cleanup();
        }      
    }
}