﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignalrDemo.Lib.ViewModel;
using SignalrDemo.Lib.Data;

namespace SignalrDemo.Lib.Messaging
{
    public class ConnectionStateChangedMessage
    {
        public ConnectionState OldState { get; set; }
        public ConnectionState NewState { get; set; }
    }

    //public class QuoteUpdatedMessage
    //{
    //    public Quote Quote { get; set; }
    //}

    public class ChatReceivedMessage
    {
        public Message message { get; set; }
    }

    public static class ConnectionStateConverter
    {
        public static ConnectionState ToConnectionState(Microsoft.AspNet.SignalR
            .Client.ConnectionState connectionState)
        {
            return (ConnectionState)Enum.Parse(typeof(ConnectionState),
                connectionState.ToString());
        }
    }
}
