﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SignalrDemo.Lib.Data
{
    public class Message : INotifyPropertyChanged
    {
        private string _sms;
        private string _userName;
        private string _roomName;

        public string Sms
        {
            get { return _sms; }
            set
            {
                if (value == _sms) return;
                _sms = value;
                OnPropertyChanged();
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                if (value == _userName) return;
                _userName = value;
                OnPropertyChanged();
            }
        }

        public string RoomName
        {
            get { return _roomName; }
            set
            {
                if (value == _roomName) return;
                _roomName = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
