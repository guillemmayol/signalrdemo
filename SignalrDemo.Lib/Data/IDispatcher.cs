﻿using System;

namespace SignalrDemo.Lib.Data
{
    public interface IDispatcher
    {
        void Dispatch(Action action);
    }
}