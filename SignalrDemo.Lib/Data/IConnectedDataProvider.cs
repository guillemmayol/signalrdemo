﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalrDemo.Lib.Data
{
    public interface IConnectedDataProvider
    {
        Task StartAsync();
        //public async Task Login(string roomName, string userName)
        Task Login(string roomName, string userName);
        void Subscribe();
        Task SendMessage(string text);
        void Stop();
    }
}
