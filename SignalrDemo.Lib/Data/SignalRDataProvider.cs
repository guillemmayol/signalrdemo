﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;
using ConnectionState = SignalrDemo.Lib.ViewModel.ConnectionState;
using SignalrDemo.Lib.Messaging;

namespace SignalrDemo.Lib.Data
{
    public class SignalRDataProvider : IConnectedDataProvider
    {
        private string _userName;
        private string _roomName;

        private HubConnection _connection;
        private IHubProxy _hubProxy;

        private const string EndpointAddress = "http://192.168.1.114:8080/";
        private const string ChatHubName = "chatHub";
        private const string ChatReceiveSmsName = "ReceiveMessage";
        //private const string ChatJoinedUserName = "UserJoinedMessage";


        private readonly IMessenger _messenger;
        private readonly IDispatcher _dispatcher;

        public SignalRDataProvider(IMessenger messenger, IDispatcher dispatcher)
        {
            _messenger = messenger;
            _dispatcher = dispatcher;
            _connection = new HubConnection(EndpointAddress);
            _hubProxy = _connection.CreateHubProxy(ChatHubName);
            //_hubProxy.On<Message>(ChatJoinedUserName, message => _dispatcher.Dispatch(() => JoinedUser()));
            //_hubProxy.On<Message>(ChatReceiveSmsName, message => _dispatcher.Dispatch(() => ReceivedSms(message)));

            _connection.StateChanged += _connection_StateChanged;
        }

        void _connection_StateChanged(StateChange stateChange)
        {
            ConnectionState oldState = ConnectionStateConverter.ToConnectionState(stateChange.OldState);
            ConnectionState newState = ConnectionStateConverter.ToConnectionState(stateChange.NewState);

            var msg = new ConnectionStateChangedMessage()
                          {
                              NewState = newState,
                              OldState = oldState,
                          };

            _dispatcher.Dispatch(() => _messenger.Send<ConnectionStateChangedMessage>(msg));
        }

        public Task StartAsync()
        {
            return _connection.Start();
        }

        
        //private void JoinedUser()
        //{
        //    var msg = "has joined the chat";
        //    _messenger.Send<Message>(msg);
        //}

        private void ReceivedSms(Message message)
        {
            var CRmsg = new ChatReceivedMessage()
            {
                message = message
            };

            _messenger.Send<ChatReceivedMessage>(CRmsg);
        }

        public void Stop()
        {
            _connection.Stop();
        }


        public Task Login(string roomName, string userName)
        {
            this._userName = userName;
            this._roomName = roomName;

            //_connection.Start();

            return _hubProxy.Invoke("JoinRoom", new object[] { _roomName, _userName }); // Provisional


            //_hubProxy.Subscribe(ChatReceiveSmsName).Received += 

            //_hubProxy.Subscribe("UserJoinedMessage").Received +=
            //    list => App.RootFrame.Dispatcher.BeginInvoke(() => Messages.Add(list[0].ToString() + "has joined the chat"));

            //_hubProxy.Subscribe("ReceivedMessage").Received +=
            //    list => App.RootFrame.Dispatcher.BeginInvoke(() => Messages.Add(list[0].ToString()));

        }

        public void Subscribe()
        {
             _hubProxy.On<Message>(ChatReceiveSmsName, message => _dispatcher.Dispatch(() => ReceivedSms(message)));
        }

        public  Task SendMessage(string text)
        {
            Message sms = new Message()
            {
                RoomName = this._roomName,
                UserName = this._userName,
                Sms = text,
            };

            return _hubProxy.Invoke("SendMessage", sms);
        }


    }
}