﻿using System;
//using GalaSoft.MvvmLight.Threading;
using SignalrDemo.Lib.Data;
using System.Windows;

namespace SignalrDemo.Wp8.Data
{
    public class PhoneDispatcher : IDispatcher
    {
        public PhoneDispatcher()
        {
            //DispatcherHelper.Initialize();
        }

        public void Dispatch(Action action)
        {
            Deployment.Current.Dispatcher.BeginInvoke(action);
            //await DispatcherHelper.RunAsync(action);
        }
    }
}