﻿using System;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;

namespace SignalrDemo.Wp8
{
    public partial class LoginPage : PhoneApplicationPage
    {
        private string _to;

        public LoginPage()
        {
            InitializeComponent();
            App.DataProvider.StartAsync();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string msg;
            if(this.NavigationContext.QueryString.TryGetValue("to",out msg))
            {  
                _to = msg;
            }  
        }

        private async void LoginClick(object sender, RoutedEventArgs e)
        {

            await App.DataProvider.Login(RoomName.Text, UserName.Text);
            App.DataProvider.Subscribe();

            if (_to.Equals(MainPage.toChat))
            {
                NavigationService.Navigate(new Uri("/ChatPage.xaml", UriKind.Relative));
            }
            else
            {
                NavigationService.Navigate(new Uri("/ShareScreenPage.xaml", UriKind.Relative));
            }
        }
    }
}