﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;

namespace SignalrDemo.Wp8
{
    public partial class ChatPage : PhoneApplicationPage
    {
        //Mouse Event Var
        private bool _dragging = false;
        private Point _start;

        public ChatPage()
        {
            InitializeComponent();
            
            // Not portable to Windows 8 better use Mouse events
            //Touch.FrameReported += new TouchFrameEventHandler(OnFrameReported);
        }

        private async void MessageClick(object sender, RoutedEventArgs e)
        {
            await App.DataProvider.SendMessage(TextBoxMessage.Text);

            TextBoxMessage.Text = string.Empty;
        }

        ////    Mouse Events

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Save the starting position of the cursor
            _start = e.GetPosition(null);

            // Capture the mouse
            LayoutRoot.CaptureMouse();
            _dragging = true;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                // Get the current position of the cursor
                Point pos = e.GetPosition(null);
                App.DataProvider.SendMouseEvent(pos.X, pos.Y, LayoutRoot.Height, LayoutRoot.Width);
            }
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Release the mouse
            LayoutRoot.ReleaseMouseCapture();
            _dragging = false;
        }

        //private async void OnFrameReported(object sender, TouchFrameEventArgs e)
        //{
            //TouchPoint point = e.GetPrimaryTouchPoint(null);
            //if (point.Action == TouchAction.Down)
            //{
            //await App.DataProvider.SendMousePosition(point.Position.X, point.Position.Y);
            //}
        //}
    }
}