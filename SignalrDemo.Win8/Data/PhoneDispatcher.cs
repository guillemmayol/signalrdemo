﻿using System;
using GalaSoft.MvvmLight.Threading;
using SignalrDemo.Lib.Data;

namespace SignalrDemo.Win8.Data
{
    public class PhoneDispatcher : IDispatcher
    {
        public PhoneDispatcher()
        {
            DispatcherHelper.Initialize();
        }

        public async void Dispatch(Action action)
        {
            await DispatcherHelper.RunAsync(action);
        }
    }
}