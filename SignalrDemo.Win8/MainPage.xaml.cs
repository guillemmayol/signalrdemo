﻿using GalaSoft.MvvmLight.Messaging;
using SignalrDemo.Lib.Data;
using SignalrDemo.Win8.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SignalrDemo.Win8
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly IConnectedDataProvider _dataProvider;

        public MainPage()
        {
            this.InitializeComponent();
            _dataProvider = new SignalRDataProvider(Messenger.Default, new PhoneDispatcher());
            _dataProvider.StartAsync();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void SendButtonClick(object sender, RoutedEventArgs e)
        {
            await _dataProvider.SendMessage(OutgoingMessage.Text);
            OutgoingMessage.Text = string.Empty;
        }

        private async void Connect_Click(object sender, RoutedEventArgs e)
        {
            await _dataProvider.Login(RoomName.Text, UserName.Text);
            _dataProvider.Subscribe();

        }
    }
}
