﻿using Microsoft.Owin.Hosting;
using System;

namespace SelfHostSignalR20
{
    class Program
    {
        static string url = "http://192.168.1.114:8080/";
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Server running on {0}", url);
                Console.ReadLine();
            }
        }
    }
}
