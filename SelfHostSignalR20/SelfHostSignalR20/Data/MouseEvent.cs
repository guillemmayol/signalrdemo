﻿
namespace SelfHostSignalR20.Data
{
    public class MouseEvent
    {
        public string UserName { get; set; }
        public string RoomName { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
    }
}
