﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SelfHostSignalR20.Data
{
    public class Message
    {
        public string Sms { get; set; }
        public string UserName { get; set; }
        public string RoomName { get; set; }
        public decimal PriceChange { get; set; }
    }
}
