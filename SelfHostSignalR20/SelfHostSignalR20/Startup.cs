﻿using Microsoft.AspNet.SignalR;
using Owin;
using System.IO;
using System.Reflection;
using SelfHostSignalR20.Hubs;

namespace SelfHostSignalR20
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Turn cross domain on 
            var config = new HubConfiguration { EnableCrossDomain = true };
            // This will map out to http://localhost:8080/signalr 
            app.MapHubs(config);
        }
    }
}
