﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SelfHostSignalR20.Data;

namespace SelfHostSignalR20.Hubs
{
    [HubName("ShareScreenHub")]
    class ShareScreenHub : Hub
    {
        public void JoinScreenSharing(string roomName, string userName)
        {
            //Check if the group exists, true -> get the id; false -> create and get id;
            Groups.Add(Context.ConnectionId, roomName);

            //thriger a fuction in the client side.
            //OtherInGroup is a dynamic object and UserJoinedMessage is a method in the client
            // So in the client we need a listener for this method.
            Clients.OthersInGroup(roomName).UserJoinedMessage(userName);
        }

        public void SendMouseEvent(MouseEvent mouseEvent)
        {
            Console.WriteLine("[" + mouseEvent.RoomName + "] " + mouseEvent.UserName + ":" + mouseEvent.X + "," + mouseEvent.Y);

            Clients.Group(mouseEvent.RoomName).ReceiveMessage(mouseEvent);

        }
    }
}
