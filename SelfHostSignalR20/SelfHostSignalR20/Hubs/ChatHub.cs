﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Linq;
using SelfHostSignalR20.Data;


namespace SelfHostSignalR20.Hubs
{
    [HubName("chatHub")]
    public class ChatHub : Hub
    {
        
        public void  JoinRoom(string roomName, string userName)
        {
            //Check if the group exists, true -> get the id; false -> create and get id;
            Groups.Add(Context.ConnectionId, roomName);

            //thriger a fuction in the client side.
            //OtherInGroup is a dynamic object and UserJoinedMessage is a method in the client
            // So in the client we need a listener for this method.
            Clients.OthersInGroup(roomName).UserJoinedMessage(userName);
        }

        //public void SendMessage(string roomName, string userName, string message)
        //{
        //    Console.WriteLine("["+ roomName + "] "+ userName + ":" + message);
        //    Clients.Group(roomName).ReceiveMessage(userName + ": " + message);
        //}
        public void SendMessage(Message message)
        {
            Console.WriteLine("["+ message.RoomName + "] "+ message.UserName + ":" + message.Sms);

            Clients.Group(message.RoomName).ReceiveMessage(message);
        }

        public void SendMousePosition(string roomName, string userName, double x, double y)
        {
            Console.WriteLine("[" + roomName + "] " + userName + "(Mouse Position)>" + x +" "+ y);
        }
    }
}